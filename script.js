const menuToggle = document.querySelector('.toggle');
const navigation = document.querySelector('.navigation');
const sec = document.querySelector('.sec');

menuToggle.addEventListener('click', function () {
	menuToggle.classList.toggle('active');
	navigation.classList.toggle('active');
	sec.classList.toggle('active');
});
